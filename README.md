# bspwm-user

A simple user-mode window manager using BSPWM.

## Setup

Create local config for bspwm & sxhkd with:

```bash
install -Dm755 /usr/share/doc/bspwm/examples/bspwmrc ~/.config/bspwm/bspwmrc
install -Dm755 /usr/share/doc/bspwm/examples/sxhkdrc ~/.config/sxhkd/sxhkdrc
git clone <repo>
cd <repo>

sudo make install
```

This will copy some default settings for sxhkd and bspwm (without them you'll
have a black screen). You can configure them however you want, that's the idea
here.

### Keyboard layout

Keyboard layut is controlled by `setxkbmap@{lang}.service` which will
automatically select your preferred language after the wm starts up.

It can also be run at any time to switch.

```bash
systemctl --user enable setxkbmap@gb
```

If you usually pass options to setxkbmap, we can do that with the environment
file which is optionally placed in `~/.config/wm.env`

```ini
XKB_OPT="caps:escape"
```

### Bars

We currently only support a single instance polybar which is enabled as
bar.service (to prevent other bars starting up).

If you like polybar this can be enabled with 

```bash
systemctl --user enable polybar
```

The idea is that we can write a config for any bar and enable it in the same
way, but only one can be enabled at once. If it's desired to have multiple
there may be a way of instancing the service and keeping a default but I've not
tried.

### Theme

If you already use pywal, this will pick up the last theme you used and apply
it on load, if not consider disabling theme.service or selecting a pywal theme.
